package sg.fintech.stocks.analyzer.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class RecordNotFoundException : Exception {

    constructor(message: String) : super(message) {}

    constructor(message: String, t: Throwable) : super(message, t) {}

    companion object {
        private val serialVersionUID = 1L
    }
}