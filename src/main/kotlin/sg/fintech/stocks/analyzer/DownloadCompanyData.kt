package sg.fintech.stocks.analyzer

import com.google.gson.Gson
import com.intrinio.api.CompanyApi
import com.intrinio.api.FundamentalsApi
import com.intrinio.invoker.ApiException
import com.intrinio.invoker.Configuration
import com.intrinio.invoker.auth.ApiKeyAuth
import com.intrinio.models.ApiResponseCompanyFundamentals
import com.intrinio.models.FundamentalSummary
import com.intrinio.models.StandardizedFinancial
import org.apache.commons.math3.stat.regression.SimpleRegression
import org.slf4j.LoggerFactory
import org.threeten.bp.LocalDate
import sg.fintech.stocks.analyzer.constants.FundamentalType
import sg.fintech.stocks.analyzer.model.entity.*
import sg.fintech.stocks.analyzer.model.service.CalculationRatioService
import sg.fintech.stocks.analyzer.model.service.CompanyNewsService
import sg.fintech.stocks.analyzer.model.service.CompanyService
import sg.fintech.stocks.analyzer.model.service.RegressionService
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.streams.toList


internal class DownloadCompanyData(
    private val threadName: String,
    private val companyEntity: CompanyEntity,
    private val api_key: String,
    private val calculationRatioService: CalculationRatioService?,
    private val regressionService: RegressionService?,
    private val companyNewsService: CompanyNewsService?,
    private val companyService: CompanyService?
) : Thread() {

    private var isThreadCompleted = true
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun run() {
        isThreadCompleted = false
        val defaultClient = Configuration.getDefaultApiClient()
        val auth = defaultClient.getAuthentication("ApiKeyAuth") as ApiKeyAuth
        auth.apiKey = api_key
        logger.info("$threadName started getStandardizedFinancialData on ${companyEntity.ticker}")
        getStandardizedFinancialData(companyEntity)
        companyEntity.require_fundamentals = "completed"
        companyService!!.createOrUpdateCompany(companyEntity)
        logger.info("$threadName completed all tasks for ${companyEntity.ticker}")
        isThreadCompleted = true
    }

    fun isCompleted(): Boolean {
        return isThreadCompleted
    }

    private fun getStandardizedFinancialData(companyEntity: CompanyEntity) {
        var ticker = companyEntity.ticker
        var fromFillingDate = companyEntity.from_filling_date
        var calRatioList = getCompanyFundamental(ticker, fromFillingDate, "FY")
        getCompanyFundamental(ticker, fromFillingDate, "QTR")

        enumValues<FundamentalType>().forEach {
            calculateRegression(calRatioList, it)
        }
    }

    private fun getCompanyFundamental(ticker: String, fromFillingDate: Date?, fiscalType: String): ArrayList<CalculationRatioEntity>{
        val companyApi = CompanyApi()
        var continueNextPage = true
        var nextPage: String? = null
        var calRatioList: ArrayList<CalculationRatioEntity> = ArrayList<CalculationRatioEntity>()
        while (continueNextPage) {
            var result: ApiResponseCompanyFundamentals?
            var retryCount = 1
            while (true) {
                try {

                    lateinit var localDate: LocalDate
                    if(fromFillingDate != null){
                        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                        localDate = LocalDate.parse(df.format(fromFillingDate))
                    }

                    localDate.toString()

                    result = companyApi.getCompanyFundamentals(ticker, null, null, null, null, "calculations", fiscalType, null, null, null, nextPage)
                    break
                } catch (e: ApiException) {
                    if(e.message == "Not Found") {
                        logger.info("getCompanyFundamentals($fiscalType) ${e.message} for $ticker")
                        result = null
                        break;
                    } else {

                        val sleepInMS = 1000
                        if (retryCount == 1) {
                            logger.info("getCompanyFundamentals($fiscalType) Too Many Requests for $ticker. retrying...")
                        }
                        sleep(sleepInMS.toLong())
                        retryCount++
                    }
                }
            }

            if (result != null) {
                if (retryCount != 1) {
                    logger.info("getCompanyFundamentals($fiscalType) re-tried $retryCount times for $ticker")
                }
                logger.info("getCompanyFundamentals($fiscalType) on $ticker, totalRecords: ${result.fundamentals.size}")
                result.fundamentals.forEach() {
                    val calRatio = saveFundamental(it, ticker)
                    if(calRatio != null){
                        calRatioList.add(calRatio)
                    }
                }

                if (result.nextPage != null) {
                    nextPage = result.nextPage
                } else {
                    continueNextPage = false
                }
            }
            else{
                continueNextPage = false
            }
        }

        return calRatioList
    }

    private fun saveFundamental(it: FundamentalSummary, ticker: String): CalculationRatioEntity? {
        try {
            val fundamentalsApi = FundamentalsApi()
            val calculations = fundamentalsApi.getFundamentalStandardizedFinancials(it.id)
            val key = CalculationRatioCompositeKey(ticker, calculations.fundamental.fiscalYear.intValueExact(), calculations.fundamental.fiscalPeriod)
            val calRatio = CalculationRatioEntity(key)

            enumValues<FundamentalType>().forEach {
                calRatio.updateField(it, getDataFromStandardizedFinancials(it, calculations.standardizedFinancials))
            }
            calculationRatioService!!.createOrUpdateCalculationRatio(calRatio)
            return calRatio
        } catch (e: ApiException) {
            System.err.println(e.message)
        }
        return null
    }

    private fun calculateRegression(calRatioList: ArrayList<CalculationRatioEntity>, fundamentalType: FundamentalType) {
        var y: List<Double?> = List<Double>(0) { 0.0 }
        var years = calRatioList.stream().map() { am -> am.composieKey.fiscalYear.toDouble() }.toList()
        if (fundamentalType == FundamentalType.adjbasicdilutedeps) {
            y = calRatioList.stream().map() { am -> am.adjbasicdilutedeps }.toList()
        } else if (fundamentalType == FundamentalType.adjbasiceps) {
            y = calRatioList.stream().map() { am -> am.adjbasiceps }.toList()
        } else if (fundamentalType == FundamentalType.adjdilutedeps) {
            y = calRatioList.stream().map() { am -> am.adjdilutedeps }.toList()
        } else if (fundamentalType == FundamentalType.adjweightedavebasicdilutedsharesos) {
            y = calRatioList.stream().map() { am -> am.adjweightedavebasicdilutedsharesos }.toList()
        } else if (fundamentalType == FundamentalType.adjweightedavebasicsharesos) {
            y = calRatioList.stream().map() { am -> am.adjweightedavebasicsharesos }.toList()
        } else if (fundamentalType == FundamentalType.adjweightedavedilutedsharesos) {
            y = calRatioList.stream().map() { am -> am.adjweightedavedilutedsharesos }.toList()
        } else if (fundamentalType == FundamentalType.altmanzscore) {
            y = calRatioList.stream().map() { am -> am.altmanzscore }.toList()
        } else if (fundamentalType == FundamentalType.apturnover) {
            y = calRatioList.stream().map() { am -> am.apturnover }.toList()
        } else if (fundamentalType == FundamentalType.arturnover) {
            y = calRatioList.stream().map() { am -> am.arturnover }.toList()
        } else if (fundamentalType == FundamentalType.assetturnover) {
            y = calRatioList.stream().map() { am -> am.assetturnover }.toList()
        } else if (fundamentalType == FundamentalType.augmentedpayoutratio) {
            y = calRatioList.stream().map() { am -> am.augmentedpayoutratio }.toList()
        } else if (fundamentalType == FundamentalType.bookvaluepershare) {
            y = calRatioList.stream().map() { am -> am.bookvaluepershare }.toList()
        } else if (fundamentalType == FundamentalType.capex) {
            y = calRatioList.stream().map() { am -> am.capex }.toList()
        } else if (fundamentalType == FundamentalType.ccc) {
            y = calRatioList.stream().map() { am -> am.ccc }.toList()
        } else if (fundamentalType == FundamentalType.commontocap) {
            y = calRatioList.stream().map() { am -> am.commontocap }.toList()
        } else if (fundamentalType == FundamentalType.compoundleveragefactor) {
            y = calRatioList.stream().map() { am -> am.compoundleveragefactor }.toList()
        } else if (fundamentalType == FundamentalType.costofrevtorevenue) {
            y = calRatioList.stream().map() { am -> am.costofrevtorevenue }.toList()
        } else if (fundamentalType == FundamentalType.croic) {
            y = calRatioList.stream().map() { am -> am.croic }.toList()
        } else if (fundamentalType == FundamentalType.currentratio) {
            y = calRatioList.stream().map() { am -> am.currentratio }.toList()
        } else if (fundamentalType == FundamentalType.debt) {
            y = calRatioList.stream().map() { am -> am.debt }.toList()
        } else if (fundamentalType == FundamentalType.debttoebitda) {
            y = calRatioList.stream().map() { am -> am.debttoebitda }.toList()
        } else if (fundamentalType == FundamentalType.debttoequity) {
            y = calRatioList.stream().map() { am -> am.debttoequity }.toList()
        } else if (fundamentalType == FundamentalType.debttonopat) {
            y = calRatioList.stream().map() { am -> am.debttonopat }.toList()
        } else if (fundamentalType == FundamentalType.debttototalcapital) {
            y = calRatioList.stream().map() { am -> am.debttototalcapital }.toList()
        } else if (fundamentalType == FundamentalType.depreciationandamortization) {
            y = calRatioList.stream().map() { am -> am.depreciationandamortization }.toList()
        } else if (fundamentalType == FundamentalType.dfcfnwc) {
            y = calRatioList.stream().map() { am -> am.dfcfnwc }.toList()
        } else if (fundamentalType == FundamentalType.dfcfnwctorev) {
            y = calRatioList.stream().map() { am -> am.dfcfnwctorev }.toList()
        } else if (fundamentalType == FundamentalType.dfnwc) {
            y = calRatioList.stream().map() { am -> am.dfnwc }.toList()
        } else if (fundamentalType == FundamentalType.dfnwctorev) {
            y = calRatioList.stream().map() { am -> am.dfnwctorev }.toList()
        } else if (fundamentalType == FundamentalType.dio) {
            y = calRatioList.stream().map() { am -> am.dio }.toList()
        } else if (fundamentalType == FundamentalType.dividendyield) {
            y = calRatioList.stream().map() { am -> am.dividendyield }.toList()
        } else if (fundamentalType == FundamentalType.divpayoutratio) {
            y = calRatioList.stream().map() { am -> am.divpayoutratio }.toList()
        } else if (fundamentalType == FundamentalType.dpo) {
            y = calRatioList.stream().map() { am -> am.dpo }.toList()
        } else if (fundamentalType == FundamentalType.dso) {
            y = calRatioList.stream().map() { am -> am.dso }.toList()
        } else if (fundamentalType == FundamentalType.earningsyield) {
            y = calRatioList.stream().map() { am -> am.earningsyield }.toList()
        } else if (fundamentalType == FundamentalType.ebit) {
            y = calRatioList.stream().map() { am -> am.ebit }.toList()
        } else if (fundamentalType == FundamentalType.ebitda) {
            y = calRatioList.stream().map() { am -> am.ebitda }.toList()
        } else if (fundamentalType == FundamentalType.ebitdagrowth) {
            y = calRatioList.stream().map() { am -> am.ebitdagrowth }.toList()
        } else if (fundamentalType == FundamentalType.ebitdamargin) {
            y = calRatioList.stream().map() { am -> am.ebitdamargin }.toList()
        } else if (fundamentalType == FundamentalType.ebitdaqoqgrowth) {
            y = calRatioList.stream().map() { am -> am.ebitdaqoqgrowth }.toList()
        } else if (fundamentalType == FundamentalType.ebitgrowth) {
            y = calRatioList.stream().map() { am -> am.ebitgrowth }.toList()
        } else if (fundamentalType == FundamentalType.ebitlesscapextointerestex) {
            y = calRatioList.stream().map() { am -> am.ebitlesscapextointerestex }.toList()
        } else if (fundamentalType == FundamentalType.ebitmargin) {
            y = calRatioList.stream().map() { am -> am.ebitmargin }.toList()
        } else if (fundamentalType == FundamentalType.ebitqoqgrowth) {
            y = calRatioList.stream().map() { am -> am.ebitqoqgrowth }.toList()
        } else if (fundamentalType == FundamentalType.ebittointerestex) {
            y = calRatioList.stream().map() { am -> am.ebittointerestex }.toList()
        } else if (fundamentalType == FundamentalType.efftaxrate) {
            y = calRatioList.stream().map() { am -> am.efftaxrate }.toList()
        } else if (fundamentalType == FundamentalType.enterprisevalue) {
            y = calRatioList.stream().map() { am -> am.enterprisevalue }.toList()
        } else if (fundamentalType == FundamentalType.epsgrowth) {
            y = calRatioList.stream().map() { am -> am.epsgrowth }.toList()
        } else if (fundamentalType == FundamentalType.epsqoqgrowth) {
            y = calRatioList.stream().map() { am -> am.epsqoqgrowth }.toList()
        } else if (fundamentalType == FundamentalType.evtoebit) {
            y = calRatioList.stream().map() { am -> am.evtoebit }.toList()
        } else if (fundamentalType == FundamentalType.evtoebitda) {
            y = calRatioList.stream().map() { am -> am.evtoebitda }.toList()
        } else if (fundamentalType == FundamentalType.evtofcff) {
            y = calRatioList.stream().map() { am -> am.evtofcff }.toList()
        } else if (fundamentalType == FundamentalType.evtoinvestedcapital) {
            y = calRatioList.stream().map() { am -> am.evtoinvestedcapital }.toList()
        } else if (fundamentalType == FundamentalType.evtonopat) {
            y = calRatioList.stream().map() { am -> am.evtonopat }.toList()
        } else if (fundamentalType == FundamentalType.evtoocf) {
            y = calRatioList.stream().map() { am -> am.evtoocf }.toList()
        } else if (fundamentalType == FundamentalType.evtorevenue) {
            y = calRatioList.stream().map() { am -> am.evtorevenue }.toList()
        } else if (fundamentalType == FundamentalType.faturnover) {
            y = calRatioList.stream().map() { am -> am.faturnover }.toList()
        } else if (fundamentalType == FundamentalType.fcffgrowth) {
            y = calRatioList.stream().map() { am -> am.fcffgrowth }.toList()
        } else if (fundamentalType == FundamentalType.fcffqoqgrowth) {
            y = calRatioList.stream().map() { am -> am.fcffqoqgrowth }.toList()
        } else if (fundamentalType == FundamentalType.fcfftointerestex) {
            y = calRatioList.stream().map() { am -> am.fcfftointerestex }.toList()
        } else if (fundamentalType == FundamentalType.finleverage) {
            y = calRatioList.stream().map() { am -> am.finleverage }.toList()
        } else if (fundamentalType == FundamentalType.freecashflow) {
            y = calRatioList.stream().map() { am -> am.freecashflow }.toList()
        } else if (fundamentalType == FundamentalType.grossmargin) {
            y = calRatioList.stream().map() { am -> am.grossmargin }.toList()
        } else if (fundamentalType == FundamentalType.interestburdenpct) {
            y = calRatioList.stream().map() { am -> am.interestburdenpct }.toList()
        } else if (fundamentalType == FundamentalType.investedcapital) {
            y = calRatioList.stream().map() { am -> am.investedcapital }.toList()
        } else if (fundamentalType == FundamentalType.investedcapitalgrowth) {
            y = calRatioList.stream().map() { am -> am.investedcapitalgrowth }.toList()
        } else if (fundamentalType == FundamentalType.investedcapitalincreasedecrease) {
            y = calRatioList.stream().map() { am -> am.investedcapitalincreasedecrease }.toList()
        } else if (fundamentalType == FundamentalType.investedcapitalqoqgrowth) {
            y = calRatioList.stream().map() { am -> am.investedcapitalqoqgrowth }.toList()
        } else if (fundamentalType == FundamentalType.investedcapitalturnover) {
            y = calRatioList.stream().map() { am -> am.investedcapitalturnover }.toList()
        } else if (fundamentalType == FundamentalType.invturnover) {
            y = calRatioList.stream().map() { am -> am.invturnover }.toList()
        } else if (fundamentalType == FundamentalType.leverageratio) {
            y = calRatioList.stream().map() { am -> am.leverageratio }.toList()
        } else if (fundamentalType == FundamentalType.ltdebtandcapleases) {
            y = calRatioList.stream().map() { am -> am.ltdebtandcapleases }.toList()
        } else if (fundamentalType == FundamentalType.ltdebttocap) {
            y = calRatioList.stream().map() { am -> am.ltdebttocap }.toList()
        } else if (fundamentalType == FundamentalType.ltdebttoebitda) {
            y = calRatioList.stream().map() { am -> am.ltdebttoebitda }.toList()
        } else if (fundamentalType == FundamentalType.ltdebttoequity) {
            y = calRatioList.stream().map() { am -> am.ltdebttoequity }.toList()
        } else if (fundamentalType == FundamentalType.ltdebttonopat) {
            y = calRatioList.stream().map() { am -> am.ltdebttonopat }.toList()
        } else if (fundamentalType == FundamentalType.marketcap) {
            y = calRatioList.stream().map() { am -> am.marketcap }.toList()
        } else if (fundamentalType == FundamentalType.netdebt) {
            y = calRatioList.stream().map() { am -> am.netdebt }.toList()
        } else if (fundamentalType == FundamentalType.netdebttoebitda) {
            y = calRatioList.stream().map() { am -> am.netdebttoebitda }.toList()
        } else if (fundamentalType == FundamentalType.netdebttonopat) {
            y = calRatioList.stream().map() { am -> am.netdebttonopat }.toList()
        } else if (fundamentalType == FundamentalType.netincomegrowth) {
            y = calRatioList.stream().map() { am -> am.netincomegrowth }.toList()
        } else if (fundamentalType == FundamentalType.netincomeqoqgrowth) {
            y = calRatioList.stream().map() { am -> am.netincomeqoqgrowth }.toList()
        } else if (fundamentalType == FundamentalType.netnonopex) {
            y = calRatioList.stream().map() { am -> am.netnonopex }.toList()
        } else if (fundamentalType == FundamentalType.netnonopobligations) {
            y = calRatioList.stream().map() { am -> am.netnonopobligations }.toList()
        } else if (fundamentalType == FundamentalType.nnep) {
            y = calRatioList.stream().map() { am -> am.nnep }.toList()
        } else if (fundamentalType == FundamentalType.noncontrolinttocap) {
            y = calRatioList.stream().map() { am -> am.noncontrolinttocap }.toList()
        } else if (fundamentalType == FundamentalType.noncontrollinginterestsharingratio) {
            y = calRatioList.stream().map() { am -> am.noncontrollinginterestsharingratio }.toList()
        } else if (fundamentalType == FundamentalType.nopat) {
            y = calRatioList.stream().map() { am -> am.nopat }.toList()
        } else if (fundamentalType == FundamentalType.nopatgrowth) {
            y = calRatioList.stream().map() { am -> am.nopatgrowth }.toList()
        } else if (fundamentalType == FundamentalType.nopatlesscapextointex) {
            y = calRatioList.stream().map() { am -> am.nopatlesscapextointex }.toList()
        } else if (fundamentalType == FundamentalType.nopatmargin) {
            y = calRatioList.stream().map() { am -> am.nopatmargin }.toList()
        } else if (fundamentalType == FundamentalType.nopatqoqgrowth) {
            y = calRatioList.stream().map() { am -> am.nopatqoqgrowth }.toList()
        } else if (fundamentalType == FundamentalType.nopattointerestex) {
            y = calRatioList.stream().map() { am -> am.nopattointerestex }.toList()
        } else if (fundamentalType == FundamentalType.normalizednopat) {
            y = calRatioList.stream().map() { am -> am.normalizednopat }.toList()
        } else if (fundamentalType == FundamentalType.normalizednopatmargin) {
            y = calRatioList.stream().map() { am -> am.normalizednopatmargin }.toList()
        } else if (fundamentalType == FundamentalType.nwc) {
            y = calRatioList.stream().map() { am -> am.nwc }.toList()
        } else if (fundamentalType == FundamentalType.nwctorev) {
            y = calRatioList.stream().map() { am -> am.nwctorev }.toList()
        } else if (fundamentalType == FundamentalType.ocfgrowth) {
            y = calRatioList.stream().map() { am -> am.ocfgrowth }.toList()
        } else if (fundamentalType == FundamentalType.ocflesscapextointerestex) {
            y = calRatioList.stream().map() { am -> am.ocflesscapextointerestex }.toList()
        } else if (fundamentalType == FundamentalType.ocfqoqgrowth) {
            y = calRatioList.stream().map() { am -> am.ocfqoqgrowth }.toList()
        } else if (fundamentalType == FundamentalType.ocftocapex) {
            y = calRatioList.stream().map() { am -> am.ocftocapex }.toList()
        } else if (fundamentalType == FundamentalType.ocftointerestex) {
            y = calRatioList.stream().map() { am -> am.ocftointerestex }.toList()
        } else if (fundamentalType == FundamentalType.operatingmargin) {
            y = calRatioList.stream().map() { am -> am.operatingmargin }.toList()
        } else if (fundamentalType == FundamentalType.opextorevenue) {
            y = calRatioList.stream().map() { am -> am.opextorevenue }.toList()
        } else if (fundamentalType == FundamentalType.oroa) {
            y = calRatioList.stream().map() { am -> am.oroa }.toList()
        } else if (fundamentalType == FundamentalType.preferredtocap) {
            y = calRatioList.stream().map() { am -> am.preferredtocap }.toList()
        } else if (fundamentalType == FundamentalType.pretaxincomemargin) {
            y = calRatioList.stream().map() { am -> am.pretaxincomemargin }.toList()
        } else if (fundamentalType == FundamentalType.pricetobook) {
            y = calRatioList.stream().map() { am -> am.pricetobook }.toList()
        } else if (fundamentalType == FundamentalType.pricetoearnings) {
            y = calRatioList.stream().map() { am -> am.pricetoearnings }.toList()
        } else if (fundamentalType == FundamentalType.pricetorevenue) {
            y = calRatioList.stream().map() { am -> am.pricetorevenue }.toList()
        } else if (fundamentalType == FundamentalType.pricetotangiblebook) {
            y = calRatioList.stream().map() { am -> am.pricetotangiblebook }.toList()
        } else if (fundamentalType == FundamentalType.profitmargin) {
            y = calRatioList.stream().map() { am -> am.profitmargin }.toList()
        } else if (fundamentalType == FundamentalType.quickratio) {
            y = calRatioList.stream().map() { am -> am.quickratio }.toList()
        } else if (fundamentalType == FundamentalType.rdextorevenue) {
            y = calRatioList.stream().map() { am -> am.rdextorevenue }.toList()
        } else if (fundamentalType == FundamentalType.revenuegrowth) {
            y = calRatioList.stream().map() { am -> am.revenuegrowth }.toList()
        } else if (fundamentalType == FundamentalType.revenueqoqgrowth) {
            y = calRatioList.stream().map() { am -> am.revenueqoqgrowth }.toList()
        } else if (fundamentalType == FundamentalType.rnnoa) {
            y = calRatioList.stream().map() { am -> am.rnnoa }.toList()
        } else if (fundamentalType == FundamentalType.roa) {
            y = calRatioList.stream().map() { am -> am.roa }.toList()
        } else if (fundamentalType == FundamentalType.roce) {
            y = calRatioList.stream().map() { am -> am.roce }.toList()
        } else if (fundamentalType == FundamentalType.roe) {
            y = calRatioList.stream().map() { am -> am.roe }.toList()
        } else if (fundamentalType == FundamentalType.roic) {
            y = calRatioList.stream().map() { am -> am.roic }.toList()
        } else if (fundamentalType == FundamentalType.roicnnepspread) {
            y = calRatioList.stream().map() { am -> am.roicnnepspread }.toList()
        } else if (fundamentalType == FundamentalType.sgaextorevenue) {
            y = calRatioList.stream().map() { am -> am.sgaextorevenue }.toList()
        } else if (fundamentalType == FundamentalType.stdebttocap) {
            y = calRatioList.stream().map() { am -> am.stdebttocap }.toList()
        } else if (fundamentalType == FundamentalType.tangbookvaluepershare) {
            y = calRatioList.stream().map() { am -> am.tangbookvaluepershare }.toList()
        } else if (fundamentalType == FundamentalType.taxburdenpct) {
            y = calRatioList.stream().map() { am -> am.taxburdenpct }.toList()
        } else if (fundamentalType == FundamentalType.totalcapital) {
            y = calRatioList.stream().map() { am -> am.totalcapital }.toList()
        }
        y = y.reversed()
        years = years.reversed()
        var x = DoubleArray(y.size) {it + 1.0}

        if (y.isNotEmpty()) {
            val simpleRegression = calculateSimpleSimpleLinearRegression(x.toList(), y)
            val comKey = RegressionCompositeKey(companyEntity.ticker, Date(System.currentTimeMillis()), fundamentalType.toString())
            val regressionDate = RegressionData(years, y)
                var gson = Gson()
                var jsonString = gson.toJson(regressionDate)

            val reg = RegressionEntity(comKey, simpleRegression.slope, simpleRegression.intercept, simpleRegression.rSquare, jsonString)
            if (!reg.m.isNaN() && !reg.c.isNaN() && !reg.r_square.isNaN()) {
                regressionService!!.createOrUpdateRegression(reg)
            }
        }
    }

    // x is always the time, i.e 2019, 2018, 2017
    // y is always the actual value i.e 1.23, 3.322, 6.33
    public fun calculateSimpleSimpleLinearRegression(x: List<Double?>, y: List<Double?>): SimpleRegression {
        val sr = SimpleRegression()
        for (i in 0 until x.count()) {
            if (y[i] != null) {
                sr.addData(x[i]!!, y[i]!!)
            }
        }

        return sr
    }

    private fun getDataFromStandardizedFinancials(tags: FundamentalType, listOfSF: List<StandardizedFinancial>): Double? {
        val financialStatement = listOfSF.find { it.dataTag.tag == tags.toString() }
        var value: Double? = null
        if (financialStatement != null && financialStatement.value != null) {
            value = financialStatement.value.toDouble()
        }
        return value
    }
}