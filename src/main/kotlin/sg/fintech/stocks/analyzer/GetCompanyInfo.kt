package sg.fintech.stocks.analyzer

import com.google.gson.Gson
import com.intrinio.api.CompanyApi
import com.intrinio.api.SecurityApi
import com.intrinio.invoker.ApiException
import com.intrinio.invoker.Configuration
import com.intrinio.invoker.auth.ApiKeyAuth
import com.intrinio.models.ApiResponseCompanies
import com.intrinio.models.CompanySummary
import com.intrinio.models.Security
import org.apache.commons.math3.stat.regression.SimpleRegression
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.Banner
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder.json
import org.threeten.bp.DateTimeUtils
import org.threeten.bp.LocalDate
import sg.fintech.stocks.analyzer.model.entity.CompanyEntity
import sg.fintech.stocks.analyzer.model.entity.RegressionData
import sg.fintech.stocks.analyzer.model.service.CalculationRatioService
import sg.fintech.stocks.analyzer.model.service.CompanyNewsService
import sg.fintech.stocks.analyzer.model.service.CompanyService
import sg.fintech.stocks.analyzer.model.service.RegressionService
import java.util.*
import kotlin.collections.ArrayList
import kotlin.streams.toList


@SpringBootApplication
class GetCompanyInfo : CommandLineRunner {

    @Autowired
    var companyService: CompanyService? = null

    @Autowired
    var calculationRatioService: CalculationRatioService? = null

    @Autowired
    var regressionService: RegressionService? = null

    @Autowired
    var companyNewService: CompanyNewsService? = null

    lateinit var api_key: String
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Throws(Exception::class)
    override fun run(vararg args: String) {

        //if (args.count() != 2) {
        //    System.err.println("Invalid parameter!")
        //    System.err.println("java sg.fintech.stocks.analyzer.DownloadFundmental APIKey numberOfThreads includeGetAllCompaniesInformation")
        //    System.exit(-1)
        //}

        this.api_key = args[0]
        if(api_key == "stop"){
            pauseAllIncomplete()
        } else {
            var includeGetAllCompaniesInformation = args[2].toBoolean()
            val defaultClient = Configuration.getDefaultApiClient()
            val auth = defaultClient.getAuthentication("ApiKeyAuth") as ApiKeyAuth
            auth.apiKey = this.api_key

            if (includeGetAllCompaniesInformation) {
                getAllCompaniesInformation("2020-02-19")
            }
            getAllCompanyRequireFundamentalsUpdates(args[1].toInt())
        }
        System.exit(0)
    }

    fun pauseAllIncomplete(){
        companyService!!.pauseAllIncomplete()
    }

    fun resumeAllIncomplete(){
        companyService!!.resumeAllIncomplete()
    }

    fun getAllCompaniesInformation(requiredDate: String?) {
        //date passed in must be in yyyy-mm-dd
        var latestFillingDate: LocalDate = LocalDate.parse("1900-01-01")
        if(requiredDate != null) {
            latestFillingDate = LocalDate.parse(requiredDate)
        }

        val companyApi = CompanyApi()
        var nextPage: String? = null
        var continueNextPage = true
        var allCompanies = ArrayList<CompanySummary>()
        while (continueNextPage) {
            var result: ApiResponseCompanies?
            var retryCount = 1
            while (true) {
                try {
                    result = companyApi.getAllCompanies(latestFillingDate, null, null, null, null, null, null, null, 1000, nextPage)
                    logger.info("Calling getAllCompanies")
                    break
                } catch (e: ApiException) {
                    val sleepInMS = 1000
                    logger.info("${e.message}, sleep $sleepInMS ms")
                    Thread.sleep(sleepInMS.toLong())
                    retryCount++
                }
            }

            if (result == null)
                throw Exception("result is empty")

            if (result.companies.size > 0) {
                allCompanies.addAll(result.companies)
                logger.info("adding ${result.companies.size} results")
            }
            if (result.nextPage != null) {
                nextPage = result.nextPage
            } else {
                continueNextPage = false
            }
        }
        logger.info("downloaded ${allCompanies.size} companies.")
        allCompanies.forEach {
            if(it.ticker != null ) {
                // save update the company to DB

                val company = CompanyEntity(id = it.id, name = it.name, ticker = it.ticker, lei = it.lei, cik = it.cik, currency = "", from_filling_date = DateTimeUtils.toSqlDate(latestFillingDate))
                logger.info("Saving ${it.ticker} ${it.name}")
                companyService!!.createOrUpdateCompany(company)

                // currentTickerNotTaken is used to track if the ticker is taken by any available thread
                // if the currentTickerNotTaken up, it will sleep for 100ms to wait for next available thread to pick up.
                // once a thread is free to take up, currentTickerNotTaken will set to false. to exit and retrieve new ticker.
//                        var currentTickerNotTaken = true
//                        while (currentTickerNotTaken) {
//                            for (i in 0 until numberOfThreads) {
//                                if (threads[i] == null || threads[i]!!.isThreadCompleted) {
//                                    threads[i] = DownloadCompanyData("DownloadCompanyFundmental-$i", it.ticker, this.api_key, calculationRatioService, regressionService, companyNewService)
//                                    threads[i]!!.start()
//                                    currentTickerNotTaken = false
//                                    break
//                                }
//                            }
//                            Thread.sleep(100)
//                        }
            }
        }
    }

    fun getAllCompanyRequireFundamentalsUpdates(numberOfThreads: Int) {
        // kotlin does not have a way to create array of size. Thus manually add to the required size
        resumeAllIncomplete()
        var threads: ArrayList<DownloadCompanyData?> = ArrayList<DownloadCompanyData?>()
        for (x in 0 until numberOfThreads) {
            threads.add(null)
        }

        var companyEntity = companyService!!.getNextCompanyRequireUpdate()

        while (companyEntity != null) {

            val securityApi = SecurityApi()
            var securityInfo: Security?
            var retryCount = 1
            while (true) {
                try {
                    securityInfo = securityApi.getSecurityById(companyEntity.ticker)
                    break
                } catch (e: ApiException) {
                    if(e.message == "Not Found") {
                        securityInfo = null
                        break;
                    };

                    val sleepInMS = 1 * 2000
                    logger.info("getSecurityById ${e.message}, sleep $sleepInMS ms")
                    Thread.sleep(sleepInMS.toLong())
                    retryCount++
                }
            }

            if(securityInfo != null){
                companyEntity.currency = securityInfo.currency
                companyEntity = companyService!!.createOrUpdateCompany(companyEntity)
            }

            var currentTickerNotTaken = true
            while (currentTickerNotTaken) {
                for (i in 0 until numberOfThreads) {
                    if (threads[i] == null || threads[i]!!.isCompleted()) {
                        companyEntity.require_fundamentals = "pending"
                        companyService!!.createOrUpdateCompany(companyEntity)
                        threads[i] = DownloadCompanyData("DownloadCompanyFundmental-$i", companyEntity, this.api_key, calculationRatioService, regressionService, companyNewService, companyService)
                        threads[i]!!.start()
                        currentTickerNotTaken = false
                        break
                    }
                }
                Thread.sleep(100)
            }
            companyEntity = companyService!!.getNextCompanyRequireUpdate()
        }

        // this to check if all the threads has completed the task.
        // one completed. removed the item from the array.
        // once array is zero mean completed all task.
        while (threads.size != 0) {
            for (i in 0 until threads.size) {
                if (threads[i] == null || threads[i]!!.isCompleted()) {
                    threads.removeAt(i)
                    break
                }
            }
            Thread.sleep(100)
        }
    }

    companion object {
        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {

            val app = SpringApplication(GetCompanyInfo::class.java)

            app.setBannerMode(Banner.Mode.OFF)
            if(args[0] == "stop") {
                val props = HashMap<String, Any>()
                props["server.port"] = 9999
                app.setDefaultProperties(props)
            }
            app.run(*args)
        }
    }
}