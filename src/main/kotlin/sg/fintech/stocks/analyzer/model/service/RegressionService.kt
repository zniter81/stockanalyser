package sg.fintech.stocks.analyzer.model.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import sg.fintech.stocks.analyzer.exception.RecordNotFoundException
import sg.fintech.stocks.analyzer.model.entity.RegressionCompositeKey
import java.util.Date
import sg.fintech.stocks.analyzer.model.entity.RegressionEntity
import sg.fintech.stocks.analyzer.model.repository.RegressionRepository

@Service
class RegressionService {

    @Autowired
    internal var repository: RegressionRepository? = null

    @Throws(RecordNotFoundException::class)
    fun getAllRegression(): List<RegressionEntity>? {
        return repository!!.findAll()
    }

    @Throws(RecordNotFoundException::class)
    fun getRegressionById(ticker: String, datetime: Date, fundamentalType: String): RegressionEntity? {
        val calculationRatio = repository!!.findById(RegressionCompositeKey(ticker, datetime, fundamentalType))

        return if (calculationRatio.isPresent) {
            calculationRatio.get()
        } else {
            null
        }
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateRegression(entity: RegressionEntity): RegressionEntity {
        val regression = getRegressionById(entity.composieKey.ticker, entity.composieKey.created_dt, entity.composieKey.fundamental_type)

        if (regression != null) {
            var newEntity = regression
            newEntity.m = entity.m
            newEntity.c = entity.c
            newEntity.r_square = entity.r_square
            newEntity.data = entity.data

            newEntity = repository!!.save(newEntity)
            return newEntity
        } else {
            return repository!!.save<RegressionEntity>(entity)
        }
    }
}