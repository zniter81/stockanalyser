package sg.fintech.stocks.analyzer.model.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import sg.fintech.stocks.analyzer.exception.RecordNotFoundException
import sg.fintech.stocks.analyzer.model.entity.CalculationRatioCompositeKey
import sg.fintech.stocks.analyzer.model.entity.CalculationRatioEntity
import sg.fintech.stocks.analyzer.model.repository.CalculationRatioRepository

@Service
class CalculationRatioService {

    @Autowired
    internal var repository: CalculationRatioRepository? = null

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateCalculationRatio(entity: CalculationRatioEntity): CalculationRatioEntity {
        return repository!!.save(entity)
    }
}