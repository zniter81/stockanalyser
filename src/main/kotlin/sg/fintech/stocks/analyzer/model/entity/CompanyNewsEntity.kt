package sg.fintech.stocks.analyzer.model.entity

import java.util.Date
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column

@Entity
@Table(name = "tb_company_news")
data class CompanyNewsEntity(
    @Id
    var id: String,

    @Column(name = "ticker")
    var ticker: String,

    @Column(name = "title")
    var title: String,

    @Column(name = "publicationDate")
    var publicationDate: Date?,

    @Column(name = "url")
    var url: String?,

    @Column(name = "summary")
    var summary: String?

)