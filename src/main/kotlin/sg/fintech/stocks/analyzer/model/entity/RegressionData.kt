package sg.fintech.stocks.analyzer.model.entity

data class RegressionData(
    val year: List<Double>,
    val value: List<Double?>
)