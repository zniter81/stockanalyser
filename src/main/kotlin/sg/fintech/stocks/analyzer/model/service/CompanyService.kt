package sg.fintech.stocks.analyzer.model.service

import java.util.ArrayList
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import sg.fintech.stocks.analyzer.exception.RecordNotFoundException
import sg.fintech.stocks.analyzer.model.entity.CompanyEntity
import sg.fintech.stocks.analyzer.model.repository.CompanyRepository

@Service
class CompanyService {

    @Autowired
    internal var repository: CompanyRepository? = null

    val allEmployees: List<CompanyEntity>
        get() {
            val employeeList = repository!!.findAll()

            return if (employeeList.size > 0) {
                employeeList
            } else {
                ArrayList<CompanyEntity>()
            }
        }

    @Throws(RecordNotFoundException::class)
    fun getCompanyById(id: String): CompanyEntity? {
        val employee = repository!!.findById(id)

        return if (employee.isPresent) {
            employee.get()
        } else {
            null
        }
    }

    @Throws(RecordNotFoundException::class)
    fun getNextCompanyRequireUpdate(): CompanyEntity? {
        return repository!!.findNextCompanyRequireFundamentals()
    }

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateCompany(entity: CompanyEntity): CompanyEntity {
        return repository!!.save(entity)
    }

    @Throws(RecordNotFoundException::class)
    fun SaveAll(entity: ArrayList<CompanyEntity>) {
        repository!!.saveAll(entity)
    }

    @Throws(RecordNotFoundException::class)
    fun pauseAllIncomplete() {
        repository!!.pauseAllIncomplete()
    }

    @Throws(RecordNotFoundException::class)
    fun resumeAllIncomplete() {
        repository!!.resumeAllIncomplete()
    }

    @Throws(RecordNotFoundException::class)
    fun deleteCompanyById(id: String) {
        val company = repository!!.findById(id)

        if (company.isPresent) {
            repository!!.deleteById(id)
        } else {
            throw RecordNotFoundException("No company record exist for given id: $id")
        }
    }
}