package sg.fintech.stocks.analyzer.model.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import sg.fintech.stocks.analyzer.model.entity.CompanyEntity
import sg.fintech.stocks.analyzer.model.entity.RegressionCompositeKey
import sg.fintech.stocks.analyzer.model.entity.RegressionEntity

@Repository
interface RegressionRepository : JpaRepository<RegressionEntity, RegressionCompositeKey>{
    @Query(
            value = "SELECT * FROM tb_company WHERE require_fundamentals = 'true' limit 0,1",
            nativeQuery = true)
    fun findNextCompanyRequireFundamentals(): RegressionEntity?
}