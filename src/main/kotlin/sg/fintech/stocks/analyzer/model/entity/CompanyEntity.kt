package sg.fintech.stocks.analyzer.model.entity

import java.util.*
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column

@Entity
@Table(name = "tb_company")
data class CompanyEntity(
    @Id
    var id: String,

    @Column(name = "ticker")
    var ticker: String,

    @Column(name = "name")
    var name: String,

    @Column(name = "lei")
    var lei: String?,

    @Column(name = "cik")
    var cik: String?,

    @Column(name = "currency")
    var currency: String?,

    @Column(name = "require_fundamentals")
    var require_fundamentals: String = "true",

    @Column(name = "from_filling_date")
    var from_filling_date: Date?
)