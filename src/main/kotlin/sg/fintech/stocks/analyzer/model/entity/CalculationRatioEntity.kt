package sg.fintech.stocks.analyzer.model.entity

import sg.fintech.stocks.analyzer.constants.FundamentalType
import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.EmbeddedId
import javax.persistence.Column
import javax.persistence.Embeddable

@Entity
@Table(name = "tb_financial_calculation_ratio")
data class CalculationRatioEntity(

    @EmbeddedId
    var composieKey: CalculationRatioCompositeKey,

    @Column(name = "adjbasicdilutedeps")
    var adjbasicdilutedeps: Double? = null,

    @Column(name = "adjbasiceps")
    var adjbasiceps: Double? = null,

    @Column(name = "adjdilutedeps")
    var adjdilutedeps: Double? = null,

    @Column(name = "adjweightedavebasicdilutedsharesos")
    var adjweightedavebasicdilutedsharesos: Double? = null,

    @Column(name = "adjweightedavebasicsharesos")
    var adjweightedavebasicsharesos: Double? = null,

    @Column(name = "adjweightedavedilutedsharesos")
    var adjweightedavedilutedsharesos: Double? = null,

    @Column(name = "altmanzscore")
    var altmanzscore: Double? = null,

    @Column(name = "apturnover")
    var apturnover: Double? = null,

    @Column(name = "arturnover")
    var arturnover: Double? = null,

    @Column(name = "assetturnover")
    var assetturnover: Double? = null,

    @Column(name = "augmentedpayoutratio")
    var augmentedpayoutratio: Double? = null,

    @Column(name = "bookvaluepershare")
    var bookvaluepershare: Double? = null,

    @Column(name = "capex")
    var capex: Double? = null,

    @Column(name = "ccc")
    var ccc: Double? = null,

    @Column(name = "commontocap")
    var commontocap: Double? = null,

    @Column(name = "compoundleveragefactor")
    var compoundleveragefactor: Double? = null,

    @Column(name = "costofrevtorevenue")
    var costofrevtorevenue: Double? = null,

    @Column(name = "croic")
    var croic: Double? = null,

    @Column(name = "currentratio")
    var currentratio: Double? = null,

    @Column(name = "debt")
    var debt: Double? = null,

    @Column(name = "debttoebitda")
    var debttoebitda: Double? = null,

    @Column(name = "debttoequity")
    var debttoequity: Double? = null,

    @Column(name = "debttonopat")
    var debttonopat: Double? = null,

    @Column(name = "debttototalcapital")
    var debttototalcapital: Double? = null,

    @Column(name = "depreciationandamortization")
    var depreciationandamortization: Double? = null,

    @Column(name = "dfcfnwc")
    var dfcfnwc: Double? = null,

    @Column(name = "dfcfnwctorev")
    var dfcfnwctorev: Double? = null,

    @Column(name = "dfnwc")
    var dfnwc: Double? = null,

    @Column(name = "dfnwctorev")
    var dfnwctorev: Double? = null,

    @Column(name = "dio")
    var dio: Double? = null,

    @Column(name = "dividendyield")
    var dividendyield: Double? = null,

    @Column(name = "divpayoutratio")
    var divpayoutratio: Double? = null,

    @Column(name = "dpo")
    var dpo: Double? = null,

    @Column(name = "dso")
    var dso: Double? = null,

    @Column(name = "earningsyield")
    var earningsyield: Double? = null,

    @Column(name = "ebit")
    var ebit: Double? = null,

    @Column(name = "ebitda")
    var ebitda: Double? = null,

    @Column(name = "ebitdagrowth")
    var ebitdagrowth: Double? = null,

    @Column(name = "ebitdamargin")
    var ebitdamargin: Double? = null,

    @Column(name = "ebitdaqoqgrowth")
    var ebitdaqoqgrowth: Double? = null,

    @Column(name = "ebitgrowth")
    var ebitgrowth: Double? = null,

    @Column(name = "ebitlesscapextointerestex")
    var ebitlesscapextointerestex: Double? = null,

    @Column(name = "ebitmargin")
    var ebitmargin: Double? = null,

    @Column(name = "ebitqoqgrowth")
    var ebitqoqgrowth: Double? = null,

    @Column(name = "ebittointerestex")
    var ebittointerestex: Double? = null,

    @Column(name = "efftaxrate")
    var efftaxrate: Double? = null,

    @Column(name = "enterprisevalue")
    var enterprisevalue: Double? = null,

    @Column(name = "epsgrowth")
    var epsgrowth: Double? = null,

    @Column(name = "epsqoqgrowth")
    var epsqoqgrowth: Double? = null,

    @Column(name = "evtoebit")
    var evtoebit: Double? = null,

    @Column(name = "evtoebitda")
    var evtoebitda: Double? = null,

    @Column(name = "evtofcff")
    var evtofcff: Double? = null,

    @Column(name = "evtoinvestedcapital")
    var evtoinvestedcapital: Double? = null,

    @Column(name = "evtonopat")
    var evtonopat: Double? = null,

    @Column(name = "evtoocf")
    var evtoocf: Double? = null,

    @Column(name = "evtorevenue")
    var evtorevenue: Double? = null,

    @Column(name = "faturnover")
    var faturnover: Double? = null,

    @Column(name = "fcffgrowth")
    var fcffgrowth: Double? = null,

    @Column(name = "fcffqoqgrowth")
    var fcffqoqgrowth: Double? = null,

    @Column(name = "fcfftointerestex")
    var fcfftointerestex: Double? = null,

    @Column(name = "finleverage")
    var finleverage: Double? = null,

    @Column(name = "freecashflow")
    var freecashflow: Double? = null,

    @Column(name = "grossmargin")
    var grossmargin: Double? = null,

    @Column(name = "interestburdenpct")
    var interestburdenpct: Double? = null,

    @Column(name = "investedcapital")
    var investedcapital: Double? = null,

    @Column(name = "investedcapitalgrowth")
    var investedcapitalgrowth: Double? = null,

    @Column(name = "investedcapitalincreasedecrease")
    var investedcapitalincreasedecrease: Double? = null,

    @Column(name = "investedcapitalqoqgrowth")
    var investedcapitalqoqgrowth: Double? = null,

    @Column(name = "investedcapitalturnover")
    var investedcapitalturnover: Double? = null,

    @Column(name = "invturnover")
    var invturnover: Double? = null,

    @Column(name = "leverageratio")
    var leverageratio: Double? = null,

    @Column(name = "ltdebtandcapleases")
    var ltdebtandcapleases: Double? = null,

    @Column(name = "ltdebttocap")
    var ltdebttocap: Double? = null,

    @Column(name = "ltdebttoebitda")
    var ltdebttoebitda: Double? = null,

    @Column(name = "ltdebttoequity")
    var ltdebttoequity: Double? = null,

    @Column(name = "ltdebttonopat")
    var ltdebttonopat: Double? = null,

    @Column(name = "marketcap")
    var marketcap: Double? = null,

    @Column(name = "netdebt")
    var netdebt: Double? = null,

    @Column(name = "netdebttoebitda")
    var netdebttoebitda: Double? = null,

    @Column(name = "netdebttonopat")
    var netdebttonopat: Double? = null,

    @Column(name = "netincomegrowth")
    var netincomegrowth: Double? = null,

    @Column(name = "netincomeqoqgrowth")
    var netincomeqoqgrowth: Double? = null,

    @Column(name = "netnonopex")
    var netnonopex: Double? = null,

    @Column(name = "netnonopobligations")
    var netnonopobligations: Double? = null,

    @Column(name = "nnep")
    var nnep: Double? = null,

    @Column(name = "noncontrolinttocap")
    var noncontrolinttocap: Double? = null,

    @Column(name = "noncontrollinginterestsharingratio")
    var noncontrollinginterestsharingratio: Double? = null,

    @Column(name = "nopat")
    var nopat: Double? = null,

    @Column(name = "nopatgrowth")
    var nopatgrowth: Double? = null,

    @Column(name = "nopatlesscapextointex")
    var nopatlesscapextointex: Double? = null,

    @Column(name = "nopatmargin")
    var nopatmargin: Double? = null,

    @Column(name = "nopatqoqgrowth")
    var nopatqoqgrowth: Double? = null,

    @Column(name = "nopattointerestex")
    var nopattointerestex: Double? = null,

    @Column(name = "normalizednopat")
    var normalizednopat: Double? = null,

    @Column(name = "normalizednopatmargin")
    var normalizednopatmargin: Double? = null,

    @Column(name = "nwc")
    var nwc: Double? = null,

    @Column(name = "nwctorev")
    var nwctorev: Double? = null,

    @Column(name = "ocfgrowth")
    var ocfgrowth: Double? = null,

    @Column(name = "ocflesscapextointerestex")
    var ocflesscapextointerestex: Double? = null,

    @Column(name = "ocfqoqgrowth")
    var ocfqoqgrowth: Double? = null,

    @Column(name = "ocftocapex")
    var ocftocapex: Double? = null,

    @Column(name = "ocftointerestex")
    var ocftointerestex: Double? = null,

    @Column(name = "operatingmargin")
    var operatingmargin: Double? = null,

    @Column(name = "opextorevenue")
    var opextorevenue: Double? = null,

    @Column(name = "oroa")
    var oroa: Double? = null,

    @Column(name = "preferredtocap")
    var preferredtocap: Double? = null,

    @Column(name = "pretaxincomemargin")
    var pretaxincomemargin: Double? = null,

    @Column(name = "pricetobook")
    var pricetobook: Double? = null,

    @Column(name = "pricetoearnings")
    var pricetoearnings: Double? = null,

    @Column(name = "pricetorevenue")
    var pricetorevenue: Double? = null,

    @Column(name = "pricetotangiblebook")
    var pricetotangiblebook: Double? = null,

    @Column(name = "profitmargin")
    var profitmargin: Double? = null,

    @Column(name = "quickratio")
    var quickratio: Double? = null,

    @Column(name = "rdextorevenue")
    var rdextorevenue: Double? = null,

    @Column(name = "revenuegrowth")
    var revenuegrowth: Double? = null,

    @Column(name = "revenueqoqgrowth")
    var revenueqoqgrowth: Double? = null,

    @Column(name = "rnnoa")
    var rnnoa: Double? = null,

    @Column(name = "roa")
    var roa: Double? = null,

    @Column(name = "roce")
    var roce: Double? = null,

    @Column(name = "roe")
    var roe: Double? = null,

    @Column(name = "roic")
    var roic: Double? = null,

    @Column(name = "roicnnepspread")
    var roicnnepspread: Double? = null,

    @Column(name = "sgaextorevenue")
    var sgaextorevenue: Double? = null,

    @Column(name = "stdebttocap")
    var stdebttocap: Double? = null,

    @Column(name = "tangbookvaluepershare")
    var tangbookvaluepershare: Double? = null,

    @Column(name = "taxburdenpct")
    var taxburdenpct: Double? = null,

    @Column(name = "totalcapital")
    var totalcapital: Double? = null

) {
    fun updateField(fieldName: FundamentalType, value: Double?) {
         when (fieldName) {
              FundamentalType.adjbasicdilutedeps -> adjbasicdilutedeps = value
              FundamentalType.adjbasiceps -> adjbasiceps = value
              FundamentalType.adjdilutedeps -> adjdilutedeps = value
              FundamentalType.adjweightedavebasicdilutedsharesos -> adjweightedavebasicdilutedsharesos = value
              FundamentalType.adjweightedavebasicsharesos -> adjweightedavebasicsharesos = value
              FundamentalType.adjweightedavedilutedsharesos -> adjweightedavedilutedsharesos = value
              FundamentalType.altmanzscore -> altmanzscore = value
              FundamentalType.apturnover -> apturnover = value
              FundamentalType.arturnover -> arturnover = value
              FundamentalType.assetturnover -> assetturnover = value
              FundamentalType.augmentedpayoutratio -> augmentedpayoutratio = value
              FundamentalType.bookvaluepershare -> bookvaluepershare = value
              FundamentalType.capex -> capex = value
              FundamentalType.ccc -> ccc = value
              FundamentalType.commontocap -> commontocap = value
              FundamentalType.compoundleveragefactor -> compoundleveragefactor = value
              FundamentalType.costofrevtorevenue -> costofrevtorevenue = value
              FundamentalType.croic -> croic = value
              FundamentalType.currentratio -> currentratio = value
              FundamentalType.debt -> debt = value
              FundamentalType.debttoebitda -> debttoebitda = value
              FundamentalType.debttoequity -> debttoequity = value
              FundamentalType.debttonopat -> debttonopat = value
              FundamentalType.debttototalcapital -> debttototalcapital = value
              FundamentalType.depreciationandamortization -> depreciationandamortization = value
              FundamentalType.dfcfnwc -> dfcfnwc = value
              FundamentalType.dfcfnwctorev -> dfcfnwctorev = value
              FundamentalType.dfnwc -> dfnwc = value
              FundamentalType.dfnwctorev -> dfnwctorev = value
              FundamentalType.dio -> dio = value
              FundamentalType.dividendyield -> dividendyield = value
              FundamentalType.divpayoutratio -> divpayoutratio = value
              FundamentalType.dpo -> dpo = value
              FundamentalType.dso -> dso = value
              FundamentalType.earningsyield -> earningsyield = value
              FundamentalType.ebit -> ebit = value
              FundamentalType.ebitda -> ebitda = value
              FundamentalType.ebitdagrowth -> ebitdagrowth = value
              FundamentalType.ebitdamargin -> ebitdamargin = value
              FundamentalType.ebitdaqoqgrowth -> ebitdaqoqgrowth = value
              FundamentalType.ebitgrowth -> ebitgrowth = value
              FundamentalType.ebitlesscapextointerestex -> ebitlesscapextointerestex = value
              FundamentalType.ebitmargin -> ebitmargin = value
              FundamentalType.ebitqoqgrowth -> ebitqoqgrowth = value
              FundamentalType.ebittointerestex -> ebittointerestex = value
              FundamentalType.efftaxrate -> efftaxrate = value
              FundamentalType.enterprisevalue -> enterprisevalue = value
              FundamentalType.epsgrowth -> epsgrowth = value
              FundamentalType.epsqoqgrowth -> epsqoqgrowth = value
              FundamentalType.evtoebit -> evtoebit = value
              FundamentalType.evtoebitda -> evtoebitda = value
              FundamentalType.evtofcff -> evtofcff = value
              FundamentalType.evtoinvestedcapital -> evtoinvestedcapital = value
              FundamentalType.evtonopat -> evtonopat = value
              FundamentalType.evtoocf -> evtoocf = value
              FundamentalType.evtorevenue -> evtorevenue = value
              FundamentalType.faturnover -> faturnover = value
              FundamentalType.fcffgrowth -> fcffgrowth = value
              FundamentalType.fcffqoqgrowth -> fcffqoqgrowth = value
              FundamentalType.fcfftointerestex -> fcfftointerestex = value
              FundamentalType.finleverage -> finleverage = value
              FundamentalType.freecashflow -> freecashflow = value
              FundamentalType.grossmargin -> grossmargin = value
              FundamentalType.interestburdenpct -> interestburdenpct = value
              FundamentalType.investedcapital -> investedcapital = value
              FundamentalType.investedcapitalgrowth -> investedcapitalgrowth = value
              FundamentalType.investedcapitalincreasedecrease -> investedcapitalincreasedecrease = value
              FundamentalType.investedcapitalqoqgrowth -> investedcapitalqoqgrowth = value
              FundamentalType.investedcapitalturnover -> investedcapitalturnover = value
              FundamentalType.invturnover -> invturnover = value
              FundamentalType.leverageratio -> leverageratio = value
              FundamentalType.ltdebtandcapleases -> ltdebtandcapleases = value
              FundamentalType.ltdebttocap -> ltdebttocap = value
              FundamentalType.ltdebttoebitda -> ltdebttoebitda = value
              FundamentalType.ltdebttoequity -> ltdebttoequity = value
              FundamentalType.ltdebttonopat -> ltdebttonopat = value
              FundamentalType.marketcap -> marketcap = value
              FundamentalType.netdebt -> netdebt = value
              FundamentalType.netdebttoebitda -> netdebttoebitda = value
              FundamentalType.netdebttonopat -> netdebttonopat = value
              FundamentalType.netincomegrowth -> netincomegrowth = value
              FundamentalType.netincomeqoqgrowth -> netincomeqoqgrowth = value
              FundamentalType.netnonopex -> netnonopex = value
              FundamentalType.netnonopobligations -> netnonopobligations = value
              FundamentalType.nnep -> nnep = value
              FundamentalType.noncontrolinttocap -> noncontrolinttocap = value
              FundamentalType.noncontrollinginterestsharingratio -> noncontrollinginterestsharingratio = value
              FundamentalType.nopat -> nopat = value
              FundamentalType.nopatgrowth -> nopatgrowth = value
              FundamentalType.nopatlesscapextointex -> nopatlesscapextointex = value
              FundamentalType.nopatmargin -> nopatmargin = value
              FundamentalType.nopatqoqgrowth -> nopatqoqgrowth = value
              FundamentalType.nopattointerestex -> nopattointerestex = value
              FundamentalType.normalizednopat -> normalizednopat = value
              FundamentalType.normalizednopatmargin -> normalizednopatmargin = value
              FundamentalType.nwc -> nwc = value
              FundamentalType.nwctorev -> nwctorev = value
              FundamentalType.ocfgrowth -> ocfgrowth = value
              FundamentalType.ocflesscapextointerestex -> ocflesscapextointerestex = value
              FundamentalType.ocfqoqgrowth -> ocfqoqgrowth = value
              FundamentalType.ocftocapex -> ocftocapex = value
              FundamentalType.ocftointerestex -> ocftointerestex = value
              FundamentalType.operatingmargin -> operatingmargin = value
              FundamentalType.opextorevenue -> opextorevenue = value
              FundamentalType.oroa -> oroa = value
              FundamentalType.preferredtocap -> preferredtocap = value
              FundamentalType.pretaxincomemargin -> pretaxincomemargin = value
              FundamentalType.pricetobook -> pricetobook = value
              FundamentalType.pricetoearnings -> pricetoearnings = value
              FundamentalType.pricetorevenue -> pricetorevenue = value
              FundamentalType.pricetotangiblebook -> pricetotangiblebook = value
              FundamentalType.profitmargin -> profitmargin = value
              FundamentalType.quickratio -> quickratio = value
              FundamentalType.rdextorevenue -> rdextorevenue = value
              FundamentalType.revenuegrowth -> revenuegrowth = value
              FundamentalType.revenueqoqgrowth -> revenueqoqgrowth = value
              FundamentalType.rnnoa -> rnnoa = value
              FundamentalType.roa -> roa = value
              FundamentalType.roce -> roce = value
              FundamentalType.roe -> roe = value
              FundamentalType.roic -> roic = value
              FundamentalType.roicnnepspread -> roicnnepspread = value
              FundamentalType.sgaextorevenue -> sgaextorevenue = value
              FundamentalType.stdebttocap -> stdebttocap = value
              FundamentalType.tangbookvaluepershare -> tangbookvaluepershare = value
              FundamentalType.taxburdenpct -> taxburdenpct = value
              FundamentalType.totalcapital -> totalcapital = value
          }
     }
}

@Embeddable
class CalculationRatioCompositeKey(

    @Column(name = "ticker")
    val ticker: String,

    @Column(name = "fiscalYear")
    val fiscalYear: Int,

    @Column(name = "fiscalPeriod")
    val fiscalPeriod: String
) : Serializable