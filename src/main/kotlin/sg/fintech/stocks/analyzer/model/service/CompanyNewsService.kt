package sg.fintech.stocks.analyzer.model.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import sg.fintech.stocks.analyzer.exception.RecordNotFoundException
import sg.fintech.stocks.analyzer.model.entity.CompanyNewsEntity
import sg.fintech.stocks.analyzer.model.repository.CompanyNewsRepository

@Service
class CompanyNewsService {

    @Autowired
    internal var repository: CompanyNewsRepository? = null

    @Throws(RecordNotFoundException::class)
    fun createOrUpdateNews(entity: CompanyNewsEntity): CompanyNewsEntity {
        return repository!!.save(entity)
    }
}