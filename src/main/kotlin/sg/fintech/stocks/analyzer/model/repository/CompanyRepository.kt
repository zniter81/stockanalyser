package sg.fintech.stocks.analyzer.model.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import sg.fintech.stocks.analyzer.model.entity.CompanyEntity

@Repository
interface CompanyRepository : JpaRepository<CompanyEntity, String>{
    @Query(
            value = "SELECT * FROM tb_company WHERE require_fundamentals = 'true' limit 0,1",
            nativeQuery = true)
    fun findNextCompanyRequireFundamentals(): CompanyEntity?

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_company SET require_fundamentals = 'pause' WHERE require_fundamentals = 'true'",
            nativeQuery = true)
    fun pauseAllIncomplete()

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_company SET require_fundamentals = 'true' WHERE require_fundamentals = 'pause'",
            nativeQuery = true)
    fun resumeAllIncomplete()
}

