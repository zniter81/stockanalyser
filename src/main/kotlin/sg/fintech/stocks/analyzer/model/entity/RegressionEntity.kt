package sg.fintech.stocks.analyzer.model.entity

import org.hibernate.annotations.TypeDef
import java.io.Serializable
import java.util.Date
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.EmbeddedId
import javax.persistence.Column
import javax.persistence.Embeddable
import com.vladmihalcea.hibernate.type.json.JsonStringType
import org.hibernate.annotations.Type

@Entity
@TypeDef(name = "json", typeClass = JsonStringType::class)
@Table(name = "tb_regression")
data class RegressionEntity(

    @EmbeddedId
    var composieKey: RegressionCompositeKey,

    @Column(name = "m")
    var m: Double,

    @Column(name = "c")
    var c: Double,

    @Column(name = "r_square")
    var r_square: Double,

    @Type(type = "json")
    @Column(name = "data", columnDefinition = "json")
    var data: String
)

@Embeddable
class RegressionCompositeKey(

    @Column
    val ticker: String,

    @Column
    val created_dt: Date,

    @Column
    val fundamental_type: String
) : Serializable