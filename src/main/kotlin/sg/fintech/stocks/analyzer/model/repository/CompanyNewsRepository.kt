package sg.fintech.stocks.analyzer.model.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import sg.fintech.stocks.analyzer.model.entity.CompanyNewsEntity

@Repository
interface CompanyNewsRepository : JpaRepository<CompanyNewsEntity, String>