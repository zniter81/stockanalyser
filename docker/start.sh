#!/usr/bin/env bash
set -euo pipefail

function cleanup() {
    docker-compose -f docker/docker-compose.local.yml down
}
trap cleanup EXIT

docker-compose -f docker/docker-compose.local.yml up